<?php

namespace Drupal\panels_breadcrumbs\Wizard;

use Drupal\page_manager\PageVariantInterface;
use Drupal\page_manager_ui\Wizard\PageEditWizard;
use Drupal\panels_breadcrumbs\Form\PageVariantBreadcrumbsForm;

/**
 * The base class for page entity form wizards.
 */
class PanelsBreadcrumbsEditWizard extends PageEditWizard {

  /**
   * {@inheritdoc}
   */
  protected function getVariantOperations(PageVariantInterface $page_variant, $cached_values) {
    $operations = parent::getVariantOperations($page_variant, $cached_values);

    $operations['breadcrumbs'] = [
      'title' => $this->t('Breadcrumbs'),
      'form' => PageVariantBreadcrumbsForm::class,
    ];

    return $operations;
  }

}
