# Panels Breadcrumbs

Panels Breadcrumbs allows you to set your breadcrumbs directly from Panels
variant configuration, and also allows you to take advantage from Panels
Arguments and Contexts as Placeholder tokens.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/panels_breadcrumbs).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/panels_breadcrumbs).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

- [Page Manager](https://www.drupal.org/project/page_manager)
- [Token](https://www.drupal.org/project/token)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Go to the Panel edit page and add breadcrumbs for required variants.


## Maintainers

- devuo - [devuo](https://www.drupal.org/u/devuo)
- David Snopek - [dsnopek](https://www.drupal.org/u/dsnopek)
- Iryna Vedkal - [IreneV](https://www.drupal.org/u/irenev)
- vasyl.kletsko - [vasylkletsko](https://www.drupal.org/u/vasylkletsko)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
